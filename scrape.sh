#!/bin/bash

DATA_DIR=`pwd`/data

if [ "$1" == "build" ]; then
  cd plpr-scraper && docker build -t scraper .
  cd ../agendas && docker build -t agendas .
  cd ../topscraper && docker build -t topics .
  cd ../mdb-merger && docker build -t merger .
fi

if [ "$1" == "" ]; then
  echo "Scraping prtotcols---------------------------------------"
  docker run -v ${DATA_DIR}:/data scraper
  echo "Finding topics-------------------------------------------"
  docker run -v ${DATA_DIR}:/data topics
  echo "Finding agendas------------------------------------------"
  docker run -v ${DATA_DIR}:/data agenda
  echo "Merging everything---------------------------------------"
  docker run -v ${DATA_DIR}:/data merger
fi
