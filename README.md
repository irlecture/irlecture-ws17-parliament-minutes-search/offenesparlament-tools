#Tools von Offenesparlamen.de

Combines the (modified) data-collection tools from offenesparlament.de

## How to use
```
sudo ./scrape.sh build
sudo ./scrape.sh
```
Will produca a data folder in the current workdir containing txt-files, json files and a sqlite3 db.

Sudo is required as the script uses docker to execute the tools.
The build command builds the required docker-containers. It can be ommitted in future runs.
